import os
import time

from pathlib import Path
from typing import List, Tuple, Set, Dict
from config import raw_dir, manual_dir, result_dir

def get_list_of_filename_dicts(raw_dir: str,
                               manual_dir: str) -> List[Dict[str, str]]:
    '''
        Gets list of dicts of key-value pairs 'filetype':'filename'

        :raw_dir: str - path to dir with raw data files
        :manual_dir: str - path to dir with manual data files
        :return: List[Dict[str, str]] - list of dicts of kv pairs
    '''
    return generate_filenames(raw_dir, manual_dir)

def check_if_non_csv_files_exist(filenames_list: str):
    check_fun = lambda filename: '.csv' in filename
    check_list = [check_fun(filename) for filename in filenames_list]
    return False in check_list


def delete_system_files_from_list(filenames: list) -> list:
    '''
      Deletes filename from list if it starts with '.'
    '''
    return [x if x[0] != '.' else None for x in filenames]
    #return [x[1:] if x[0] == '.' else x for x in filenames]

def get_new_filenames(dir_path: str) -> List:
    old_filenames = get_actual_filenames(dir_path)
    rename_func = lambda x: x.replace('.txt', '')
    return [rename_func(filename) for filename in old_filenames]


def get_actual_filenames(dir_path: str) -> List:
    files = [str(filename) for filename in Path(dir_path).iterdir()]
    return delete_system_files_from_list(files)
    #return [x[1:] if x[0] == '.' else x for x in files ]

#print(get_actual_filenames('raw_data/'))


def check_both_files_exist(raw_file_path: str,
                           manual_file_path: str) -> bool:
    '''
      Check if both file raw/manual exist
      returns bool value 

      :raw_file_path: str - path to raw data file
      :manual_file_path: str - path to manual data file
      :return: bool - True/False value if both files exists
    '''
    raw_path = Path(raw_file_path)
    manual_path = Path(manual_file_path)
    return raw_path.is_file() and manual_path.is_file()


def get_match_intersection(raw_dir: str,
                           manual_dir: str) -> Set[Tuple[str, str]]:
    '''
      Gets filenames of !EXISTED! files in raw and manual data dirs.
      Extract set with identity: Tuple(date, sname) of files 
      Returns the intersection of raw and manual identities
      (only identities of existed file pairs)

      :raw_dir: str - raw data dir path
      :manual_dir: str - manual data dir path
      :return: Set[Tuple[str, str]] - set of file identities
    '''

    raw_files = get_actual_filenames(raw_dir)
    manual_files = get_actual_filenames(manual_dir)
    
    raw_ids = {get_file_identity_tuple(x) for x in raw_files}
    manual_ids = {get_file_identity_tuple(x) for x in manual_files}
    return raw_ids.intersection(manual_ids)


def date_from_filename(filename: str) -> str:
    '''
      Returns the str with date from full filename

      :filename: str - full filename
      :return: str - string with date (DD.MM.YY)
    '''
    return filename.split('_')[-1][:-4]


def sname_from_filename(filename: str) -> str:
    '''
       Returns the str with surname from full filename

       :filename: str - full filename
       :return: str - string with surname
    '''
    return filename.split('_')[1].split('/')[1]


def get_file_identity_tuple(filename: str) -> Tuple[str, str]:
    '''
      Returns the file 'identity' - pair of strings -
      (date, surname)

      :filename: str - full filename
      :return: Tuple[date: str, sname: str] - file identity
    '''
    date = date_from_filename(filename)
    sname = sname_from_filename(filename)
    return (date, sname)


def restore_filename_from_identity(type: str,
                                   identity: Tuple[str, str]) -> str:
    '''
      Returns the full filename from inputted identity and file type 

      :type: str - (raw, manual, result) type of file to create
      :return: str - full filename (sname_type_date.csv)
    '''
    return identity[1] + f'_{type}_' + identity[0] + '.csv'


def make_filenames_dict(identity: Tuple[str, str]) -> Dict[str, str]:
    '''
      Returns dict with three key-value pairs filetype : filename

      :identity: Tuple[str, str] - file identity
      :return: Dict[str, str] ({
                                'raw': 'sname_raw_date.csv',
                                'manual': 'sname_manual_date.csv',
                                'result': 'sname_result_date.csv'
                              })
    '''
    raw_file = restore_filename_from_identity('raw', identity)
    manual_file = restore_filename_from_identity('manual', identity)
    result_file = restore_filename_from_identity('result', identity)
    filenames_dict = {
                      'raw': raw_file,
                      'manual': manual_file,
                      'result': result_file
                      }
    return filenames_dict


def generate_filenames(raw_dir: str,
                       manual_dir: str) -> List[Dict[str, str]]:
    '''
      Generates list of dicts from the set of file identities
      (only files that exists and have pairs)

      :raw_dir: str - raw data dir
      :manual_dir: str - manual data dir
      :returns: List[Dict[str, str]] - [{
                                        'raw': 'sname_raw_date.csv',
                                        'manual': 'sname_manual_date.csv',
                                        'result': 'sname_result_date.csv'
                                        }, ...]
    '''
    ids_to_restore = get_match_intersection(raw_dir, manual_dir)
    filenames_list = [make_filenames_dict(id) for id in ids_to_restore]
    return filenames_list

