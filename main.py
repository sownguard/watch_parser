import imp
import os
import traceback
from match_files import match_data_files
from service import clear_result_dir
from manage_data import get_list_of_filename_dicts
from config import raw_dir, manual_dir, result_dir

clear_result_dir(result_dir)
for filename_dict in get_list_of_filename_dicts(raw_dir, manual_dir):
    try:
        print(f"Doing files: {filename_dict['raw'], filename_dict['manual']}")
        match_data_files(filename_dict)
        processed_files = (filename_dict['raw'], filename_dict['manual'])
    except Exception as e:
        print(traceback.format_exc(), f'''\n {filename_dict['raw']}''')
    
    try:
        os.remove(raw_dir + processed_files[0])
        os.remove(manual_dir + processed_files[1])
    except Exception as e:
        print(traceback.format_exc(), 'ERROR while deleting data files')
    print('\n End. start fetching new files')
