import csv

from typing import Generator

class CSV_iterator():
    def __init__(self,
                 filename: str) -> None:
        self.filename = filename

    def init_gen(self) -> Generator:
        ''' 
            Gets csv_iterator from file passed to CSV_iterator
            instance.
            :return: Generator  - generator object over csv_iterator
        '''

        fp = open(self.filename, 'r', encoding='utf-8')
        csv_iterator = csv.reader(fp, delimiter = ',')
        if not self._selfcheck(csv_iterator):
            csv_iterator = csv.reader(fp, delimiter = ';')
        return csv_iterator

    
    def _selfcheck(self,
                   gen: Generator) -> bool:
        '''
            Checks if csv file parsed with proper delimiter
            
            :return: bool
        '''
        check_row = next(gen)
        return len(check_row) > 1
