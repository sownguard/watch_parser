import csv
import sys
import os
import traceback

from datetime import datetime
from typing import List, Tuple, Generator, Dict, Any
from pathlib import Path

from csv_iterator import CSV_iterator
from time_operate import time_from_full_timestamp, time_from_short_timestamp
from manage_data import generate_filenames
from service import clear_result_dir, create_result_file
from config import raw_dir, manual_dir, result_dir


def get_csv_generator(filename: str) -> Generator:
    '''
        Returns the generator object of CSV_iterator
        
        :filename: str - full filename
        :return Generator
    '''
    return CSV_iterator(filename).init_gen()


def write_rows(target_file: str,
               to_write: List[List[str]]) -> None:
    ''' 
        Writes List with result data to csv file

        :target_file: str - filename to write data in
        :to_write: List[List[str]] - list of lists with
          resulting data
    '''

    target_file_path = result_dir + target_file
    try:
        with open(target_file_path, 'x', encoding='utf-8') as ap:
            writer = csv.writer(ap)
            writer.writerows(to_write)
    except FileExistsError as existfile:
        with open(target_file_path, 'a', encoding='utf-8') as ap:
            writer = csv.writer(ap)
            writer.writerows(to_write)


def process_rows(raw_row: List[str],
                 manual_row: List[str],
                 raw_gen: Generator, 
                 manual_gen: Generator,
                 to_write: List[List[str]]) -> Dict[str, List[Any]]:
    '''
        Compares timestamps from raw data and manual data rows.
        If raw timestamp fits time interval from manual row - 
        gets the activity state from manual, appends in to raw
        data row.

        :raw_row: List[str] - row from raw data file
        :manual_row: List[str] -  row from manual data file
        :raw_gen: Generator over CSV_iterator instance of raw file
        :manual_gen: Generator over CSV_iterator instance of manual file
        :to_write: List[str] - list of raw rows
                   that fits to tome intervals from manual rows.
                   Used to store raw rows that have to be written to
                   result file
        :return: Dict[str, List[str] | List[List[str]]] - 3 key-value pairs
                 of {'raw_row': raw_row,
                    'manual_row': manual_row,
                    'to_write': to_write }
    '''
    
    raw_ts = time_from_full_timestamp(raw_row[0])
    m_start = time_from_short_timestamp(manual_row[0]) 
    m_end = time_from_short_timestamp(manual_row[1]) 
    m_state = manual_row[2]

    # Uncomment to debug in case of infinite execution.
    # print(f'Last processing timestamps:\n {raw_ts} \n {m_start} \n {m_end}')

    if raw_ts < m_start:
        try:
            raw_row = next(raw_gen)
        except Exception as e:
            print(e, f'\n {raw_row}')

    elif (raw_ts >= m_start) and (raw_ts <= m_end):
        raw_row.append(m_state)
        to_write.append(raw_row)
        raw_row = next(raw_gen)
    
    elif (raw_ts > m_start) and (raw_ts > m_end):
        manual_row = next(manual_gen)
    
    return {'raw_row': raw_row,
             'manual_row': manual_row,
             'to_write': to_write }


def match_data_files(filenames: dict) -> None:
    '''
        Mathches data between filenames['raw'] and filenames ['manual']
        files. If timestamp from raw file fits the time interval in
        manual file - append the activity state from manual file
        to the end of the row of raw data file

        :filenames: List[Dict[str, str]] - list of dicts of key-value
                    pairs 'filetype':'filename'
    '''
    manual_gen = get_csv_generator(manual_dir + filenames['manual'])
    raw_gen = get_csv_generator(raw_dir + filenames['raw'])
    create_result_file(filenames['result'])
    to_write: List[str]= []
    manual_row = next(manual_gen)
    raw_row = next(raw_gen)
    res = {'raw_row':raw_row,
           'manual_row':manual_row,
            'to_write':to_write}

    while True:
        try:
            # Call function in loop, recursively using arguments.
            # Arguments used to call the function came from previous
            # iteration of function execution
            res = process_rows( res['raw_row'], 
                                res['manual_row'],
                                raw_gen,
                                manual_gen,
                                res['to_write'])
            if len(res['to_write']) > 5000:
                try:
                    write_rows(filenames['result'], res['to_write'])
                    res['to_write'] = []
                except Exception as e:
                    print('\n ERROR - Cant write data to result file \n ', e)
                
        except StopIteration:
            write_rows(filenames['result'], res['to_write'])
            res['to_write'] = []
            break


