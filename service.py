import os
import time

from manage_data import get_actual_filenames, get_new_filenames


def rename_non_csv_files(dir_path: str) -> list:
    '''
        Removes 
    '''
    old_filenames = get_actual_filenames(dir_path)
    new_filenames = get_new_filenames(dir_path)
    for old, new in zip(old_filenames, new_filenames):
        if not '.csv' in old:
            try:
                os.rename(old, new + '.csv')
            except FileNotFoundError as notfound:
                print(f'Cant rename file {old}', notfound)
                pass


def clear_result_dir(result_dir_path: str) -> None:
    '''
        Removes all csv files from resuld_data dir

        :result_dir_path: str - path to result dir data
    '''
    actual_files = get_actual_filenames(result_dir_path)
    if len(actual_files) != 0:
        print('Cleaning the result dir...')
        for file in actual_files:
            try:
                os.remove(file)
            except FileNotFoundError:
                print(f'Cant find file {file} to delete\n')
        time.sleep(3)


def create_result_file(filename: str,
                        result_dir : str = 'result_data/') -> None:
    '''
      Creates result csv file if not exist.
      If exists truncating it.
      
      :filename: str - filename to create
    '''
    file_path = result_dir + filename
    try:
        with open(file_path, 'x', encoding='utf-8') as fp:
            fp.write('')
    except FileExistsError:
        with open(file_path, 'w', encoding='utf-8') as fp:
            fp.write('')

rename_non_csv_files('raw_data/')
