from datetime import datetime, time
from typing import Any

from csv_iterator import CSV_iterator
from config import raw_dir, manual_dir, result_dir

def time_from_full_timestamp(full_timestamp: str) -> Any:
    '''
        Extracts time from full timestamp.

        :full_timestamp: str - full timestamp from 
            raw data file
        :return: datetime.time
    '''

    full_timestamp = full_timestamp[:-1]
    try:
        dtm_time = datetime.strptime(full_timestamp,
         '%Y-%m-%d %H:%M:%S.%f').time()
        return dtm_time
    except ValueError as valueerror:
        print(f'\n ERROR - time_from_full_timestamp function - {full_timestamp}', valueerror)
        return None
    except UnboundLocalError as unbounderr:
        print('\n ERROR - time_from_full_timestamp function - ', unbounderr)
        return None
    except Exception as exc:
        print('\n ERROR - time_from_full_timestamp function - IDK WHAT THIS', exc)
        return None


def time_from_short_timestamp(short_timestamp: str) -> Any:
    '''
        Extracts time from short timestamp.

        :full_timestamp: str - short timestamp from 
            manual data file
        :return: datetime.time / None in case of error
    '''
    
    try:
        dtm_time = datetime.strptime(short_timestamp, '%H:%M').time()
        return dtm_time
    except ValueError as valueerror:
        print('\n ERROR - time_from_short_timestamp function - ', valueerror)
        return None
    except UnboundLocalError as unbounderr:
        print('\n ERROR - time_from_short_timestamp function - ', unbounderr)
        return None
    except Exception as exc:
        print('\n ERROR - time_from_short_timestamp function - IDK WHATS THIS', exc)
        return None
